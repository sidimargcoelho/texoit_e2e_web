#encoding: utf-8
#language:pt

  @albums
  Funcionalidade: Link albums

    Esquema do Cenario: Validar os dados do objeto com id 6
      Dado que acessei o menu 'Guide'
      Quando quando acesso o link '/albums/1/photos'
      Então eu valido os "<dados>" do objeto com id 6
      Exemplos:
        | dados |
        | "accusamus ea aliquid et amet sequi nemo" |
        | "https://via.placeholder.com/600/56a8c2" |
        | "https://via.placeholder.com/150/56a8c2" |

