Dado('que acessei o menu {string}') do |menu|
  @ap.click_guide menu
end

Quando('quando acesso o link {string}') do |link|
  @ap.click_guide link
end

Então('eu valido os "{string}" do objeto com id {int}') do |string, int|
  @ap.content_json
  @ap.valid_array_json(string,int)
end

