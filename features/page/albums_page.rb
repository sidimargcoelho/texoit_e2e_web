class AlbumPage < SitePrism::Page

  def click_guide(value)
    scroll_to(text: value,align: :top)
    click_on (value)
  end

  def content_json
    url = 'https://jsonplaceholder.typicode.com/albums/1/photos'
    uri = URI(url)
    response = HTTParty.get(uri)
    @dados = JSON.parse(response.body)
  end

  def valid_array_json(value,value_id)
    @dados.each_with_index do |data,index|
      if data['id'] == value_id
        RSpec::Matchers.describe(data)do
          it {is_expected.to match value}
          end
        end
      end
    end

  end




