require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'faker'
require 'rspec'
require 'site_prism'
require 'httparty'

World (Capybara::DSL)

BROWSER = ENV['BROWSER']
ENVIRONMENT_TYPE = ENV['ENVIRONMENT_TYPE']

case BROWSER
when 'chrome'
  @driver = :selenium_chrome
when "headless"
  Capybara.register_driver :selenium_chrome_headless do |app|
    Capybara::Selenium::Driver.new(app,
                                   :browser => :chrome,
                                   :desired_capabilities => Selenium::WebDriver::Remote::Capabilities.chrome(
                                     'chromeOptions' => {
                                       'args' => [ "headless", "window-size=1280x720", "disable-gpu"]
                                     }
                                   )
    )
  end
  @driver = :selenium_chrome_headless
else
  puts "Navegador invalido"
end
Capybara.configure do |config|
  config.default_driver = @driver
  Capybara.page.driver.browser.manage.window.maximize
  config.default_max_wait_time = 3
end