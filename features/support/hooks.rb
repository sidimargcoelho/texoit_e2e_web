Before do
  Faker::Config.locale= 'pt-BR'
  visit 'https://jsonplaceholder.typicode.com'
  @ap = AlbumPage.new
end

After do |scenario|
  scene_name = scenario.name.gsub(/[^A-Za-z0-9]/ , '')
  scene_name =scene_name.gsub(' ', '_').downcase!
  screenshot = "log/screenshots/#{scene_name}.png"
  page.save_screenshot(screenshot)
end


